﻿using System;
using _2._9_HomeWork_Solid_Zverev.I.I.Interfaces;

namespace _2._9_HomeWork_Solid_Zverev.I.I
{
    public class NumberGame: INumberGame
    {
        private bool _gameStarted;

        private bool _gameFinished;

        private int _tryCount;

        private int _tryLimit;

        private int _secretNumber;

        private readonly INumberChecker _numberChecker;

        private readonly NumberGenerator _numberGenerator;

        public NumberGame(NumberGenerator numberGenerator, INumberChecker numberChecker, int trylimit)
        {
            _tryLimit = trylimit;
            _numberGenerator = numberGenerator;
            _secretNumber = _numberGenerator.GetSecretNumber();
            _numberChecker = numberChecker;
            _gameStarted = true;
            _gameFinished = false;
        }

        public string CheckNumber(int number)
        {
            _tryCount++;
            string result = string.Empty;
            NumberCheckerResult check = _numberChecker.CheckNumber(_secretNumber,number);
            if (check == NumberCheckerResult.less) { result = "Указаное число меньше"; }
            if (check == NumberCheckerResult.greater) { result = "Указаное число больше"; }
            if (check == NumberCheckerResult.equal) { _gameFinished = true;  result = "Угадано!"; }
            if (_tryCount == _tryLimit) { _gameFinished = true;  result = "Закончились попытки"; }
            return result;
        }

        public string StopGame()
        {
            _gameStarted = false;
            return "Игра: \"Угадай число\" остановлена";
        }

        public string StartGame()
        {
            _gameStarted = true;
            return "Игра: \"Угадай число\" запущена";
        }

        public bool CkeckGameIsRunning()
        {
            return _gameStarted;
        }

        public bool CheckGameFinished()
        {
            return _gameFinished;
        }

        public int GetTryCount()
        {
            return _tryCount;
        }

        public int GetSecretNumber()
        {
            return _secretNumber;
        }

        public int GetTryLimit()
        {
            return _tryLimit;
        }

    }
}

