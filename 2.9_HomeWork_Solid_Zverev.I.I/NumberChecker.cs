﻿using System;
using _2._9_HomeWork_Solid_Zverev.I.I.Interfaces;

namespace _2._9_HomeWork_Solid_Zverev.I.I
{
    public class NumberChecker: INumberChecker
    {
        public NumberCheckerResult CheckNumber(int secretNumber,int number)
        {
            if (secretNumber < number) { return NumberCheckerResult.less; }
            if (secretNumber > number) { return NumberCheckerResult.greater; }
            return NumberCheckerResult.equal;
        }
    }
}
