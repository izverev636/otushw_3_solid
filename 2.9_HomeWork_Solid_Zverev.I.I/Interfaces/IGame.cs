﻿using System;
namespace _2._9_HomeWork_Solid_Zverev.I.I.Interfaces
{
    public interface IGame
    {
        public string StartGame();

        public string StopGame();

        public bool CkeckGameIsRunning();

        public bool CheckGameFinished();
    }
}

