﻿using System;
namespace _2._9_HomeWork_Solid_Zverev.I.I.Interfaces
{
    public interface INumberChecker
    {
        NumberCheckerResult CheckNumber(int secretNumber, int number);
    }
}

public enum NumberCheckerResult
{
    equal = 0,
    less = 1,
    greater = 2
}

