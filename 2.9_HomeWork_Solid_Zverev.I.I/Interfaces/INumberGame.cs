﻿using System;
namespace _2._9_HomeWork_Solid_Zverev.I.I.Interfaces
{
    public interface INumberGame: IGame
    {
        public int GetSecretNumber();

        public int GetTryCount();

        public int GetTryLimit();

        public string CheckNumber(int Number);

    }
}

