﻿using System;
namespace _2._9_HomeWork_Solid_Zverev.I.I
{
    public class NumberGeneratorStorage : NumberGenerator
    {
        private IEnumerable<int> _generatedNumbers;

        public NumberGeneratorStorage(int min, int max) : base(min,max)
        {
        }

        public override int GetSecretNumber()
        {
            if (_generatedNumbers is null) { _generatedNumbers = new List<int>(); }
            int newNumber = base.GetSecretNumber();
            _generatedNumbers.Append(newNumber);
            return newNumber;
        }
    }
}

